#!/bin/bash
# 脚本解释器 强制设置为 bash
if [ "$BASH" != "/bin/bash" -a "$BASH" != "/usr/bin/bash" ]; then
   bash $0 "$@"
   exit $?
fi

function error(){
    echo -e "\033[1;31m"$1"\033[0m"
        exit 1
}
# root权限执行此脚本
[ "${UID}" -ne 0 ] && error "请使用root权限执行"

CURRENT_DIR=$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)
cd ${CURRENT_DIR}
# CPU1上预留的cpu数
RESERVE_CPU1=4
# CPU2上预留的cpu数
RESERVE_CPU2=0
# GPU是否跨NUMA。即是否忽略numa，monbox实例完全平均到GPU上。
GPU_across_NUMA=true

# GPU数量
NUM_OF_GPU=$(lspci|grep AMD|grep 5100|wc -l)
# 所有的cpu数
ALL_CPU=$(lscpu|grep -w "CPU(s)"|head -n 1|awk '{print $2}')
# numa_node数
NUM_OF_NUMA=$(lscpu|grep "NUMA node(s)"|awk '{print $3}')
# 启动xorg
for((i=0;i<${NUM_OF_GPU};i++))
do
        ps -ef|grep -v "grep"|grep xorg${i}
        if [ $? -ne 0 ]
        then
                nohup Xorg :${i} -config /root/xorg${i}.conf -sharevts &
                sleep 5
        fi
done
# 判断是否有第一个参数
if [ -z $1 ]
then
    error "usage: <start_num> [end_num]"
fi
# 第一个参数是最小数量(MIN)
MIN=$1
# 判断是否有第二个参数，第二个参数为最大数量(MAX)
if [ -z $2 ]
then
   MAX=$MIN
else
   MAX=$2
fi

# 输出创建的实例数量或范围
if [ $MIN -lt $MAX ]
then
        echo "start monbox and video_stream_server $MIN ~ $MAX"
elif [ $MIN -eq $MAX ]
then
        echo "start monbox and video_stream_server $MIN"
else
        error "start_num must be less than end_num"
fi

pci_ids=$(lspci|grep AMD|grep 5100|awk '{print $1}')
j=0
k=0
s=0
for pci_id in ${pci_ids}
do
        if [ ${#pci_id} -eq 7 ]
        then
                r_pci_id="0000:${pci_id}"
        else
                r_pci_id="${pci_id}"
        fi
        gpu_node=$(ls /sys/bus/pci/devices/${r_pci_id}/drm/|grep renderD)
        numa_node=$(cat /sys/bus/pci/devices/${r_pci_id}/numa_node)
        echo "${pci_id}--${gpu_node}--${numa_node}"
        all_gpu[s]=${gpu_node}
        let s++
        if [ ${numa_node} -lt $((NUM_OF_NUMA/2)) ]
        then
                cpu1[j]=${gpu_node}
                let j++
        else
                cpu2[k]=${gpu_node}
                let k++
        fi
done
cpu1_gpu_num=${#cpu1[@]}
cpu2_gpu_num=${#cpu2[@]}
all_gpu_num=${#all_gpu[@]}
if [ ! ${GPU_across_NUMA} ]
then
        if [ ${cpu1_gpu_num} -eq 0 ]
        then
                RESERVE_CPU1=$((${ALL_CPU}/2))
        fi
        if [ ${cpu2_gpu_num} -eq 0 ]
        then
                RESERVE_CPU2=$((${ALL_CPU}/2))
        fi
fi
NUM_OF_CPU1=$(((${ALL_CPU}/2)-${RESERVE_CPU1}))
NUM_OF_CPU2=$(((${ALL_CPU}/2)-${RESERVE_CPU2}))
# 多少组CPU核 一组4个
GROUP_OF_CPU1=$((${NUM_OF_CPU1}/4))
GROUP_OF_CPU2=$((${NUM_OF_CPU2}/4))
GROUP_OF_CPU=$((${GROUP_OF_CPU1}+${GROUP_OF_CPU2}))

for i in `seq ${MIN} ${MAX}`
do
        echo "---------$i----------"
                starttime=$(date +'%Y-%m-%d %H:%M:%S')
                echo "starttime = ${starttime}"
                start_seconds=$(date --date="${starttime}" +%s);
        MONBOX_NAME=monbox_$i
                # 计算绑cpu
        if [ $((($i-1)%${GROUP_OF_CPU})) -lt ${GROUP_OF_CPU1} ]
        then
                CPU_START=$((((($i-1)%${GROUP_OF_CPU})*4)+${RESERVE_CPU1}))
        else
                CPU_START=$((((($i-1)%${GROUP_OF_CPU})*4)+${RESERVE_CPU2}+${RESERVE_CPU1}))
        fi
        CPU_END=$((${CPU_START}+3))
        CPUS="${CPU_START}-${CPU_END}"
        echo "CPUS = ${CPUS}"
                # 计算绑numa和GPU
        if [ ${CPU_END} -lt $((${ALL_CPU}/2)) ]
        then
                if [ ${NUM_OF_NUMA} = 4 ]
                                then
                                        MEM="0,1"
                                elif [ ${NUM_OF_NUMA} = 2 ]
                                then
                                        MEM="0"
                                else
                                        echo "numa node(s):${NUM_OF_NUMA} ??"
                                        exit 1
                                fi
                                
        else
                                if [ ${NUM_OF_NUMA} = 4 ]
                                then
                                        MEM="2,3"
                                elif [ ${NUM_OF_NUMA} = 2 ]
                                then
                                        MEM="1"
                                else
                                        echo "numa node(s):${NUM_OF_NUMA} ??"
                                        exit 1
                                fi
                                
        fi
                if [ ${GPU_across_NUMA} ]
                then
                        n=$((${i}%${all_gpu_num}))
				else
					if [ ${CPU_END} -lt $((${ALL_CPU}/2)) ]
					then
						n=$((${i}%${cpu1_gpu_num}))
					else
						n=$((${i}%${cpu2_gpu_num}))
					fi
                fi
                # GPU="renderD128"
        echo "MEM : $MEM"
        echo "GPU -- $n"
                # 计算虚拟机port
                port=$((5554+2*(i-1)))
        echo "${port}"

                # 创建虚拟机
                android create avd --name test_$i --target android-24 --abi arm64-v8a --device "Nexus 4" --skin "720x1280" --sdcard 500M --force
                sleep 5
                # 导入环境变量
				echo "${n}"
                export DISPLAY=:${n}
                # 启动虚拟机

                nohup numactl -C $CPUS -m $MEM emulator -avd test_$i -port $port -partition-size 4096 -no-window -cores 2 -gpu host -qemu --enable-kvm -m 2048 -vnc :$i &
				#sleep 5
                #if [ $? -eq 0 ]
	            #then
			#		count_time=0
			#		while true
			##		do
			#			adb -s emulator-$port shell getprop sys.boot_completed|grep 1 > /dev/null 2>&1
			#			if [ $? -eq 0 ]
			#			then
			#				echo "emulator-$port started successfully"
			#				break
			#			fi
						# 60秒未成功启动超时跳过
			#			if [ ${count_time} -gt 60 ]
		#				then
		#					echo -e "\033[1;31mStart check timed out,emulator-$port unable to start\033[0m"
		#					echo -e "\033[1;31memulator-$port started failed\033[0m"
		#					break
		#				fi
		#				sleep 1
		#				let count_time++
		#			done
		#		else
		#			echo -e "\033[1;31memulator-$port started failed\033[0m"
		#		fi
				
                endtime=$(date +'%Y-%m-%d %H:%M:%S')
                echo "endtime = ${endtime}"
                end_seconds=$(date --date="${endtime}" +%s);
                echo "本次运行时间："$((end_seconds-start_seconds))"s"
done

echo ""
echo "--------------done-------------"
exit 0

