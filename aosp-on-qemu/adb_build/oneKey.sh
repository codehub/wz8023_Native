branch=android-7.0.0_r35


if [ ! -d "external" ]; then
  mkdir external
fi

cd external

if [ ! -d "boringssl" ]; then
  git clone -b $branch https://android.googlesource.com/platform/external/boringssl
  git reset --hard b57f38e6a8884da65b1dfdab19c9e15a9ab0a74e
fi
if [ ! -d "compiler-rt" ]; then
  git clone -b $branch https://android.googlesource.com/platform/external/compiler-rt
  git reset --hard fa6e8d21528c9d085d1565cef2cea204fe9d6a2d
fi
if [ ! -d "libcxx" ]; then
  git clone -b $branch https://android.googlesource.com/platform/external/libcxx
  git reset --hard 8fd719529a85a9379dfe0095bf193e76481bb805
fi
if [ ! -d "libcxxabi" ]; then
  git clone -b $branch https://android.googlesource.com/platform/external/libcxxabi
  git reset --hard ee6e479d12ee68d10102042e46f9599757e37e63
fi
if [ ! -d "libusb" ]; then
  git clone -b $branch https://android.googlesource.com/platform/external/libusb
  git reset --hard dc5d19652007b3ea00631f6753afa9e04aea9960
fi
if [ ! -d "mdnsresponder" ]; then
  git clone -b $branch https://android.googlesource.com/platform/external/mdnsresponder
  git reset --hard 8539179a11a22401e92c3ba1f3e5cad83d28789c
fi


if [ ! -d "../system" ]; then
  mkdir ../system
fi

cd ../system

if [ ! -d "core" ]; then
  git clone -b $branch https://android.googlesource.com/platform/system/core
  git reset --hard 806e0278d25a2b25b82b4ed5b38cd9e4cbc5bebe
fi

cd ..

if [ ! -f ".sconsign.dblite" ]; then
  patch -p1 -m < adb.patch
fi

scons && cp _adb adb

echo "adb is under current path"
