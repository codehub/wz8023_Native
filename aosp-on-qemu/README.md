#   基于鲲鹏的云手机QEMU解决方案移植构建指导

#    （本文档仅为内部项目组参考使用）

**本文注意事项：注意每个章节操作步骤的构建环境操作系统，请按照文档中标注的操作系统进行构建**



[TOC]



# 1    背景和文档目的

本文档目的是指导如何构建基于鲲鹏框架的云手机解决方案，编译环境需要2台服务器:1台鲲鹏920平台的编译环境, 1台X86平台的编译环境，构建生成最终运行在鲲鹏平台上的android-sdk-linux软件包。

# 2    QEMU编译环境和构建步骤

## 2.1  鲲鹏920环境准备和组件编译构建（for tools 和 platform-tools builded）

### 2.1.1  鲲鹏920上编译环境准备

编译环境鲲鹏920搭建：操作系统ubuntu18.04.1的鲲鹏920服务器(物理机)，在其上安装ubuntu16.04.04的虚拟机，ubuntu16.04.04的虚拟机用于编译emulator及相关依赖组件（虚拟机的内存至少大于16G）。

#### 2.1.1.1 鲲鹏920的系统安装

ubuntu18.04.1的iso下载链接：

下载位置：http://old-releases.ubuntu.com/releases/18.04.1/ubuntu-18.04.1-server-arm64.iso

鲲鹏920（物理机）上安装ubuntu18.04的安装指导：

参考： https://support.huawei.com/enterprise/zh/doc/EDOC1100052765 

#### 2.1.1.2 鲲鹏920上安装ubuntu16.04.04的虚拟机安装

 鲲鹏920上虚拟机安装指导和编译环境安装：

参考仓内的aosp-on-qemu/docForUserInstructionManual/ubuntuOS_virtual_installer_for_the_HI1620.docx 

#### 2.1.1.3 镜像源

````
deb http://cmc-cd-mirror.rnd.huawei.com/ubuntu-ports/ xenial main restricted universe multiverse
deb http://cmc-cd-mirror.rnd.huawei.com/ubuntu-ports/ xenial-security main restricted universe multiverse
deb http://cmc-cd-mirror.rnd.huawei.com/ubuntu-ports/ xenial-updates main restricted universe multiverse
deb http://cmc-cd-mirror.rnd.huawei.com/ubuntu-ports/ xenial-proposed main restricted universe multiverse
deb http://cmc-cd-mirror.rnd.huawei.com/ubuntu-ports/ xenial-backports main restricted universe multiverse
deb-src http://cmc-cd-mirror.rnd.huawei.com/ubuntu-ports/ xenial main restricted universe multiverse
deb-src http://cmc-cd-mirror.rnd.huawei.com/ubuntu-ports/ xenial-security main restricted universe multiverse
deb-src http://cmc-cd-mirror.rnd.huawei.com/ubuntu-ports/ xenial-updates main restricted universe multiverse
deb-src http://cmc-cd-mirror.rnd.huawei.com/ubuntu-ports/ xenial-proposed main restricted universe multiverse
deb-src http://cmc-cd-mirror.rnd.huawei.com/ubuntu-ports/ xenial-backports main restricted universe multiverse

````

#### 2.1.1.4安装编译所需依赖

````shell
apt update
apt -y install build-essential autoconf xfce4 xubuntu-desktop xrdp openjdk-8-jre ccache
apt -y install libxt-dev libx11-dev libssl-dev libgl1-mesa-dev libpulse-dev
apt -y install libswt-gtk-4-java libswt-gtk-4-jni libswt-cairo-gtk-4-jni libswt-glx-gtk-4-jni libswt-gnome-gtk-4-jni libswt-webkit-gtk-4-jni
apt -y install libtool libgbm-dev gettext-doc libepoxy-dev libfdt-dev libpng++-dev
apt -y install llvm-3.5 clang flex bison scons libedit-dev ant libudev-dev
apt -y install git git-core curl repo openjdk-8-jdk-headless
ln -sf /usr/lib/aarch64-linux-gnu/libedit.so.2 /usr/lib/aarch64-linux-gnu/libedit.so
ln -sf /usr/bin/llvm-config-3.5  /usr/bin/llvm-config
````



###  2.1.2  构建（android-sdk-linux）**<u>tools目录</u>**下部件

发布的版本中会附带代码仓中aosp-on-qemu目录，将aosp-on-qemu文件夹，上传至鲲鹏920上的“ubuntu16.04.04虚拟机”内。

代码仓根目录拷贝到：

```shell
cp -r  aosp-on-qemu      /home/$(YOUR_USER)/
```

android-sdk-linux项目目录构建，该目录用于存放最后编译出来的所有目标二进制文件：

```shell
mkdir –p /home/$(YOUR_USER)/android-sdk-linux //创建目标目录，后文记为$(ANDROID_SDK_LINUX_DIR)
cd $(ANDROID_SDK_LINUX_DIR)
```

```shell
mkdir tools                //tools中将存放模拟器相关部件
mkdir platforms            //platforms中将存放安卓平台API部件
mkdir system-images        //system-images中将存放编译出来的安卓系统镜像
mkdir platform-tools       //platform-tools中将存放adb二进制可执行文件
```

#### 2.1.2.1 emulator源码编译及拷贝到tools下

编译环境（参见2.1.1.2章节）：鲲鹏920服务器上的“ubuntu16.04.04虚拟机”。

#### 2.1.2.2 emulator代码下载（版本emu-master-dev）

```shell
mkdir -p /home/$(YOUR_USER)/$(YOUR_EmuMasterDev_DIR)  //android模拟器源码主目录
cd $(YOUR_EmuMasterDev_DIR)
repo init -u https://android.googlesource.com/platform/manifest -b emu-master-dev
repo sync -j32

//拷贝仓中根目录下 aosp-on-qemu/binaryAndFiles/snapshot_qemu_emu_B630.xml 快照文件到.repo/manifests/目录下，对qemu的所有git仓进行代码回退
cp snapshot_qemu_emu_B630.xml  /home/$(YOUR_USER)/$(YOUR_EmuMasterDev_DIR)/.repo/manifests/
cd /home/$(YOUR_USER)/$(YOUR_EmuMasterDev_DIR)
repo init -m snapshot_qemu_emu_B630.xml
repo sync -d -j32           
```

确保其中的$(YOUR_EmuMasterDev_DIR)/external/qemu代码仓，回退到指定提交commit号：0e8e684fba9eadcc112067359f9f1027659f3918

```shell
cd external/qemu
git log
//确认最后一次提交commit号为0e8e684fba9eadcc112067359f9f1027659f3918
```

#### 2.1.2.3 emulator依赖库构建

##### 2.1.2.3.1 删除Prebuilts下x86自带库文件mesa,qt,ANGLE：

```shell
cd $(YOUR_EMUMASTERDEV_DIR)/prebuilts/android-emulator-build
rm -rf qt mesa
cd common
rm -rf ANGLE
```

##### 2.1.2.3.2 脚本安装编译所需依赖库

【下列组件的curl,qemu-deps,e2fsprogs,lz4,libvpx,libxml2,libsub,ffmpeg,x264,virglrenderer编译】

获取代码仓根目录下的补丁：aosp-on-qemu/patchForEmuMasterDev/0002_prebuilds.patch：

```shell
cd $(YOUR_EMUMASTERDEV_DIR)/external/qemu/
patch -p1 < 0002_prebuilds.patch
```

运行脚本库文件（curl,qemu-deps,e2fsprogs,lz4,libvpx,libxml2,libsub,ffmpeg,x264,virglrenderer）：

```shell
cd $(YOUR_EMUMASTERDEV_DIR)/external/qemu/android/scripts/
./build-curl.sh
./build-qemu-android-deps.sh
./build-e2fsprogs.sh
./build-ffmpeg.sh
./build-lz4.sh
./build-libvpx.sh
./build-libxml2.sh 
./build-libusb.sh
chmod +x build-x264.sh
./build-x264.sh
./build-virglrenderer.sh
```

##### 2.1.2.3.3 安装库文件(protobuf,breakpad)

**1 protobuf安装**

 执行安装脚本，代码仓根目录下： aosp-on-qemu/oneKey/build_protobuf.sh

```shell
./build_protobuf.sh  $(YOUR_EMUMASTERDEV_DIR)
```

//注意输入参数$(YOUR_EMUMASTERDEV_DIR)

**2 breakpad 安装**

 执行安装脚本，代码仓根目录下： aosp-on-qemu/oneKey/build_breakpad.sh

```shell
./build_breakpad.sh  $(YOUR_EMUMASTERDEV_DIR)
```

//注意输入参数$(YOUR_EMUMASTERDEV_DIR)

##### 2.1.2.3.4 编译Qt 5.6.1：

下载qt5.6.1源码，（版本为：5.6.1）内部onebox链接：https://onebox.huawei.com/p/91be8850d937f376742bf4fba97b4cc2

解压qt-everywhere-opensource-src-5.6.1.tar.xz

```shell
tar -xvJf qt-everywhere-opensource-src-5.6.1.tar.xz
```

进入qt-everywhere-opensource-src-5.6.1目录：

```shell
cd qt-everywhere-opensource-src-5.6.1
```

修改文件：qtimageformats/src/3rdparty/libwebp/src/dsp/dsp.h，注释第68行 //#define WEBP_USE_NEON

执行configure生成Makefile。（备注：在生成Makefile时会报：Makefile:177: recipe for target 'xxx' failed和make: *** [xxx] Error 1为正常现象）

```shell
./configure -v -opensource -confirm-license -force-debug-info -release -no-rpath -shared -nomake examples -nomake tests -no-strip -qt-xcb -c++std c++11 -qt-pcre -no-glib
```

编译并安装：

```shell
make -j64 && make install
```

创建qt依赖库目录：

```shell
cd $(YOUR_EMUMASTERDEV_DIR)/prebuilts/android-emulator-build
mkdir -p qt/linux-aarch64/plugins
mkdir -p qt/linux-aarch64/lib
mkdir -p qt/linux-aarch64/bin
mkdir -p qt/common/include
cd qt/linux-aarch64
ln -sf ../common/include include
```

拷贝QT5.6.1到emulator的prebuilts目录下：

```shell
cd $(YOUR_EMUMASTERDEV_DIR)/prebuilts/android-emulator-build
cp -r /usr/local/Qt-5.6.1/lib/* qt/linux-aarch64/lib/
cp -r /usr/local/Qt-5.6.1/bin/* qt/linux-aarch64/bin/
cp -r /usr/local/Qt-5.6.1/plugins/* qt/linux-aarch64/plugins/
cp -r /usr/local/Qt-5.6.1/include/* qt/common/include/
```

##### 2.1.2.3.4 源码编译Mesa 19.0.8-0ubuntu0~18.04.3

打入补丁，代码仓根目录下：aosp-on-qemu/patchForEmuMasterDev/0003_build_mesa.patch：

```shell
cd $(YOUR_EMUMASTERDEV_DIR)/external/qemu
patch -p1 < 0003_build_mesa.patch
./android/scripts/build-mesa-deps.sh
./android/scripts/build-mesa.sh
```

##### 2.1.2.3.5 源码编译ANGLE

编译安装ninja工具，（版本：1.7.2）内部onebox链接：https://onebox.huawei.com/p/91be8850d937f376742bf4fba97b4cc2

安装ninja：

```shell
tar -xf ninja-1.7.2.tar.gz
cd /root/ninja-1.7.2
./configure.py  --bootstrap
cp -r ninja /usr/bin
```

运行脚本build-ANGLE.sh执行编译：

```shell
cd $(YOUR_EMUMASTERDEV_DIR)/external/qemu/android/scripts/
./build-ANGLE.sh
执行完成后对angle代码进行回退
cd /tmp/root-build-ANGLE-xxx/build-linux-aarch64/angle
git reset --hard c37c696b94bd7ddecc12a2c3192f5e48eae7842e
ninja -C out/Debug
mkdir /home/angle
cp -r /tmp/root-build-ANGLE-xxx/build-linux-aarch64/angle/out/Debug/obj /home/angle
cp -r /tmp/root-build-ANGLE-xxx/build-linux-aarch64/angle/out/Debug/lib /home/angle
修改ANGLE脚本：build-ANGLE.sh，在脚本run ninja -C out/Debug处后面加一行，此行内容如下
cp -r /home/angle/*  $PKG_BUILD_DIR/out/Debug/
在执行一遍build-ANGLE.sh
./build-ANGLE.sh
```

【异常情况处理！】如angle编译异常时编译方法如下：

第一次运行脚本build-ANGLE.sh生成目录/tmp/root-build-ANGLE-xxx/build-linux-aarch64/angle,执行命令ninja -C out/Debug生成.so及.a文件

```
./build-ANGLE.sh 
cd /tmp/root-build-ANGLE-xxx/build-linux-aarch64/angle
git reset --hard c37c696b94bd7ddecc12a2c3192f5e48eae7842e
ninja -C out/Debug
mkdir /home/angle
cp -r /tmp/root-build-ANGLE-xxx/build-linux-aarch64/angle/out/Debug/obj /home/angle
cp -r /tmp/root-build-ANGLE-xxx/build-linux-aarch64/angle/out/Debug/lib /home/angle
```

修改ANGLE脚本：build-ANGLE.sh，在脚本run ninja -C out/Debug处后面加一行，此行内容如下：

```
cp -r /home/angle/*  $PKG_BUILD_DIR/out/Debug/
```

第二次运行脚本build-ANGLE.sh执行编译：

```
./build-ANGLE.sh
```



##### 2.1.2.3.7 emulator代码打补丁

代码仓根目录下补丁：aosp-on-qemu/patchForEmuMasterDev/0001_emuMasterDev_qemu.patch

补丁拷贝到qemu目录下并合入：

```shell
cp 0001_emuMasterDev_qemu.patch $(YOUR_EMUMASTERDEV_DIR)/external/qemu
cd $(YOUR_EMUMASTERDEV_DIR)/external/qemu
git checkout . //合入补丁前保证代码干净
patch -p1 < 0001_emuMasterDev_qemu.patch
```

##### 2.1.2.3.8 emulator编译生成objs及拷贝到tools

**环境变量设置(重要步骤！)**

```shell
export LD_LIBRARY_PATH=$(YOUR_EMUMASTERDEV_DIR)/prebuilts/android-emulator-build/qt/linux-aarch64/lib/ 
```

**编译**

```shell
cd $(YOUR_EMUMASTERDEV_DIR)/external/qemu
```

若存在objs目录，说明这份代码之前有人编译过，首先删除其他人的编译信息，若不存在，则跳过下面两条命令。

```shell
make clean
rm –rf objs
```

**不带符号表版本编译**（易于debug）

```shell
./android/configure.sh
make -j64
```

**带符号表版本编译**

```shell
./android/configure.sh --symbols
make -j64
```

拷贝objs到tools目录，

编译成功以后，将objs目录下的内容拷贝到android-sdk-linux目录下的tools目录中：

```shell
rm -rf $(YOUR_EMUMASTERDEV_DIR)/external/qemu/objs/build
cp -r $(YOUR_EMUMASTERDEV_DIR)/external/qemu/objs/* $(ANDROID_SDK_LINUX_DIR)/tools/
```

#### 2.1.2.4 swt.jar源码编译及拷贝到tools下

编译环境（参见2.1.1.2章节）: 服务器鲲鹏920上的ubuntu16.04.04虚拟机；java版本：openjdk version "1.8.0_222"。

创建临时目录：

```shell
mkdir –p /home/$(YOUR_USER)/TEMP
```

代码仓根目录下：/aosp-on-qemu/oneKey中，获取swt-build.sh脚本，并放入TEMP目录中：

```shell
cd TEMP
./swt-build.sh   //执行脚本构建swt.jar，生成位置TEMP/swt_dir/swt.jar
```

swt.jar拷贝至指定目录：

```shell
mkdir $(ANDROID_SDK_LINUX_DIR)/tools/lib/aarch64
cp swt.jar  $(ANDROID_SDK_LINUX_DIR)/tools/lib/aarch64/
```

#### 2.1.2.5 mksdcard源码编译及拷贝到tools

编译环境（参见2.1.1.2章节）: 服务器鲲鹏920上的ubuntu16.04.04虚拟机

```shell
cd $(YOUR_EMUMASTERDEV_DIR)/sdk/emulator/mksdcard/src/source
gcc -o mksdcard mksdcard.c
```

将生成的mksdcard文件拷贝到 android-sdk-linux/tools/目录下：

```shell
cp mksdcard  $(ANDROID_SDK_LINUX_DIR)/tools
```

#### 2.1.2.6 ICU+PNG 4个.so库替换

编译环境（参见2.1.1.2章节）: 服务器鲲鹏920上的ubuntu16.04.04虚拟机。

四个库是：libicui18n.so  libicui18n.so.52  libicui18n.so.52.1 libpng12.so.0 。

代码仓根目录下：aosp-on-qemu/oneKey中，获取icu-libpng12-build.sh脚本，放在TEMP目录中：

```shell
cd /home/$(YOUR_USER)/TEMP
./icu-libpng12-build.sh
```

将TEMP/build-dir/icu/source/lib/ 目录生成的 libicui18n.so  libicui18n.so.52  libicui18n.so.52.1 三个库文件  拷贝到$(ANDROID_SDK_LINUX_DIR)/tools/lib64/qt/lib/ 目录下：

```shell
cp -r TEMP/build-dir/icu/source/lib/*  $(ANDROID_SDK_LINUX_DIR)/tools/lib64/qt/lib/
```

将TEMP/build-dir/icu/source/libpng-1.2.50/.libs/libpng12.so.0  一个文件拷贝到$(ANDROID_SDK_LINUX_DIR)/tools/lib64/ 目录下：

```shell
cp -r TEMP/build-dir/icu/source/libpng-1.2.50/.libs/* $(ANDROID_SDK_LINUX_DIR)/tools/lib64/  
```

#### 2.1.2.7 android及jar包拷贝

编译环境（参见2.1.1.2章节）: 服务器鲲鹏920上的ubuntu16.04.04虚拟机。

下载x86上适用的android-sdk-linux.tgz压缩包，并解压到用户目录下/home/(YOUR_USER)，下载地址: http://dl.google.com/android/android-sdk_r24.4.1-linux.tgz：

```shell
mkdir -p /home/$(YOUR_USER)/TEMP_DIR
cd TEMP_DIR
wget http://dl.google.com/android/android-sdk_r24.4.1-linux.tgz
tar -xf android-sdk_r24.4.1-linux.tgz
cp TEMP_DIR/android-sdk-linux/tools/android $(ANDROID_SDK_LINUX_DIR)/tools/
cp TEMP_DIR/android-sdk-linux/tools/lib/*.jar $(ANDROID_SDK_LINUX_DIR)/tools/lib/
```

### 2.1.3  构建（android-sdk-linux）**<u>platform-tools目录</u>**下部件

编译环境（参见2.1.1.2章节）: 服务器鲲鹏920上的ubuntu16.04.04虚拟机。
代码仓根目录下：/aosp-on-qemu/adb_build目录复制到自己指定的目录。
已有编译好的adb文件在/aosp-on-qemu/adb_build目录下直接拷贝到android-sdk-linux/platform-tools/使用或者可以放到arm服务器侧的/usr/bin下使用

```shell
cp -r adb_build $(PATH_TO_AS_YOU_LIKE)
cd $(PATH_TO_AS_YOU_LIKE)
sh oneKey.sh
```

adb可执行文件生成在$(PATH_TO_AS_YOU_LIKE)，拷贝到android-sdk-linux/platform-tools/ 

```shell
cp adb android-sdk-linux/platform-tools/
```



## 2.2  x86编译环境准备和组件编译构建（for platforms 和 system-images builded）

### 2.2.1 编译环境x86准备

编译环境x86搭建：操作系统ubuntu16.04.0x的x86服务器，用于编译aosp及相关组件。

#### 2.2.1.1 操作系统ubuntu16.04.03下载

下载位置：http://old-releases.ubuntu.com/releases/16.04.3/ubuntu-16.04.3-server-amd64.iso

#### 2.2.1.2 安装编译所需依赖（x86）

```shell
apt install openjdk-8-jdk
apt install libx11-dev:i386 libreadline6-dev:i386 libgl1-mesa-dev g++-multilib 
apt install git flex bison gperf build-essential libncurses5-dev:i386 
apt install tofrodos python-markdown libxml2-utils xsltproc zlib1g-dev:i386 
apt install dpkg-dev libsdl1.2-dev libesd0-dev
apt install git-core gnupg flex bison gperf build-essential  
apt install zip curl zlib1g-dev gcc-multilib g++-multilib 
apt install libc6-dev-i386 
apt install lib32ncurses5-dev x11proto-core-dev libx11-dev 
apt install libgl1-mesa-dev libxml2-utils xsltproc unzip m4
apt install lib32z-dev ccache
apt install bc python flex bison gperf libsdl-dev libesd0-dev build-essential zip curl
```

### 2.2.2 构建（android-sdk-linux）**<u>platforms目录</u>**下部件

编译环境（2.2.1搭建）：应用服务器x86，操作系统ubuntu16.04.0x。

#### 2.2.2.1 android studio下载

从谷歌官方获取，从http://tools.android-studio.org/ ，下载android-studio-ide-183.5522156-linux.tar.gz，解压到用户目录下/home/$(YOUR_USER)：

```shell
cd /home/$(YOUR_USER)
```

```shell
tar xvf android-studio-ide-183.5522156-linux.tar.gz
cd $(USER)/android-studio/bin
./studio.sh
```

#### 2.2.2.2 适配android7.0(android-24)

启动android studio，打开“File”->"Settings"，选择“Appearance & Behavior”->"System Settings"->"Android SDK"，选择“SDK Platforms”勾选“Android 7.0(Nougat)”并点击OK按钮下载。

下载后，在“Appearance & Behavior”->"System Settings"->"Android SDK"页中，"Android SDK Location"为指定的目录位置，记为(YOUR_ANDROID_SDK_LOCATION)，文件或文件夹包含共计12个：android-stubs-src.jar、android.jar、build.prop、data、framework.aidl、optional、sdk.properties、skins、source.properties、templates、uiautomator.jar。

将YOUR_ANDROID_SDK_LOCATION/platforms/目录下整个android-24目录拷贝到鲲鹏920编译服务器上android-sdk-linux/platforms/目录下。

#### 2.2.2.3 适配android9.0(android-28)

启动android studio，打开“File”->"Settings"，选择“Appearance & Behavior”->"System Settings"->"Android SDK"，选择“SDK Platforms”勾选“Android 9.0(Pie)”并点击OK按钮下载。

下载后，在“Appearance & Behavior”->"System Settings"->"Android SDK"页中，"Android SDK Location"为指定的目录位置，记为(YOUR_ANDROID_SDK_LOCATION)，文件或文件夹包含共计12个：android-stubs-src.jar、android.jar、build.prop、data、framework.aidl、optional、sdk.properties、skins、source.properties、templates、uiautomator.jar。

将YOUR_ANDROID_SDK_LOCATION/platforms/目录下整个android-28目录拷贝到鲲鹏920服务器上android-sdk-linux/platforms/ 目录下。

#### 2.2.2.4 Android内核编译

编译ndk下载安装：

从https://developer.android.google.cn/ndk/downloads/older_releases.html  

下载android-ndk-r16b-linux-x86_64.zip 并解压到/home/$ (YOUR_DIR)/android-ndk-r16b ：

```shell
mkdir -p /home/$(YOUR_DIR)/AndroidToolChain/arm64  
cd /home/$(YOUR_DIR)/android-ndk-r16b/  
./build/tools/make_standalone_toolchain.py --arch arm64 --api 24 --install /home/$(YOUR_DIR)/AndroidToolChain/arm64  --force  
export ARM64=$(YOUR_DIR)/AndroidToolChain/arm64/bin  
export PATH=$ARM64:$PATH  
export ARCH=arm64  
export CROSS_COMPILE=aarch64-linux-android-  
export LD=aarch64-linux-android-ld
```

下载Android(安卓)内核源码（android7.0和android9.0系统使用同一份安卓内核！）：

```shell
mkdir -p /home/$(YOUR_KERNEL)
cd /home/$(YOUR_KERNEL)
git clone  https://android.googlesource.com/kernel/goldfish  
cd goldfish
git checkout android-goldfish-4.4-dev
确保切入分支，后续合入补丁基于此分支：f4355abac45a608dbe0e7d87fd50c349b90d6928
git checkout f4355abac45a608dbe0e7d87fd50c349b90d6928
```

<u>***【注意】***此处依据硬件平台确定是否合入32 bit转64 bit的内核补丁**</u>

```shell
若"运行平台为鲲鹏916"， 则以下合入转码补丁步骤不执行!
合入32位转64位的补丁,补丁goldfish-4.4-dev.patch来源：
仓中文件位置：aosp-on-qemu/binaryAndFiles/Exagear_version_20191111_AOSP9.0/Exagear_V100R002C00SPC011B001/exagear-a32a64-vm.tar.gz
tar zxvf exagear-a32a64-vm.tar.gz  
解压后将ExaGear ARM32-ARM64 for Android VM/kernel/目录下goldfish-4.4-dev.patch补丁文件拷贝
到/home/$(YOUR_KERNEL)/goldfish/目录下
patch -p1 -m < goldfish-4.4-dev.patch   //32 bit 转码补丁合入
```

编译kernel：

```shell
make ranchu64_defconfig
make -j8
```

拷贝内核二进制文件：

```shell
cd arch/arm64/boot
```

文件Image即为所需Android内核，重命名为kernel-ranchu，拷贝到鲲鹏916/920云手机运行环境中，

在鲲鹏920编译环境的android-sdk-linux目录下创建镜像存放的目录：

```shell
cd $(ANDROID_SDK_LINUX_DIR)
mkdir -p system-images/android-24（or 28）/default/arm64-v8a
```

将kernel-ranchu放到$(ANDROID-SDK-LINUX_DIR)/system-images/android-24（or 28）/default/arm64-v8a目录下。

### 2.2.3 构建（android-sdk-linux）**<u>system-images目录</u>**下部件

编译环境（2.2.1搭建）：应用服务器x86，操作系统ubuntu16.04.0x。

####  2.2.3.1 构建aosp-7.0.0_r35的system-images目录（支持鲲鹏916服务器）

##### 2.2.3.1.1 下载aosp源码（android-7.0.0_r35）

```shell
mkdir -p /home/$(AOSP_DIR)  
cd /home/$(AOSP_DIR)  
repo init -u https://android.googlesource.com/platform/manifest -b android-7.0.0_r35   
repo sync
//拷贝仓中更目录下 aosp-on-qemu/binaryAndFiles/snapshot_aosp7_B630.xml 快照文件到.repo/manifests/目录下，对aosp7的所有git仓进行代码回退
cp snapshot_aosp7_B630.xml  /home/$(AOSP_DIR)/.repo/manifests/
cd /home/$(AOSP_DIR)
repo init -m snapshot_aosp7_B630.xml
repo sync -d -j32  
cd /home/$(AOSP_DIR)  
source build/envsetup.sh  
lunch aosp_arm64-eng
```

##### 2.2.3.1.2 合入0001_patchaosp7r35.patch补丁 

代码仓根目录下：aosp-on-qemu/patchForAosp7.0.0r35/0001_patchForAosp7r35.patch 

```shell
patch -p1 < 0001_patchForAosp7r35.patch
```

##### 2.2.3.1.3 aosp7.0.0r35镜像编译

```shell
cd /home/$(AOSP_DIR)
make clean  
make -j64
```

编译成功后，在$(AOSP_DIR)/out/target/product/generic_arm64可以看到如下最新生成的4个如下文件：  

cache.img  ramdisk.img  system.img  userdata.img。

##### 2.2.3.1.4 system-images目录创建及img文件拷贝

将x86上编译所得的4个img(system.img、userdata.img、ramdisk.img、cache.img)文件拷贝到鲲鹏920编译环境上的$( ANDROID_SDK_LINUX_DIR)/system-images/android-24/default/arm64-v8a/目录下。

代码仓根目录下：aosp-on-qemu/patchForEmuMasterDev/source.properties拷贝到指定目录，$( ANDROID_SDK_LINUX_DIR)/system-images/android-24/default/arm64-v8a/。

####  2.2.3.2 构建aosp-7.0.0_r35的system-images目录（支持鲲鹏920服务器）

##### 2.2.3.2.1 下载aosp源码（android-7.0.0_r35）

```shell
mkdir -p /home/$(AOSP_DIR)  
cd /home/$(AOSP_DIR)  
repo init -u https://android.googlesource.com/platform/manifest -b android-7.0.0_r35   
repo sync 
//拷贝仓中更目录下 aosp-on-qemu/binaryAndFiles/snapshot_aosp7_B630.xml 快照文件到.repo/manifests/目录下，对aosp7的所有git仓进行代码回退
cp snapshot_aosp7_B630.xml  /home/$(AOSP_DIR)/.repo/manifests/
cd /home/$(AOSP_DIR)
repo init -m snapshot_aosp7_B630.xml
repo sync -d -j32
cd /home/$(AOSP_DIR)  
source build/envsetup.sh  
lunch aosp_arm64-eng
```

##### 2.2.3.2.2 合入0001_patchaosp7r35.patch补丁 

代码仓根目录下：aosp-on-qemu/patchForAosp7.0.0r35/0001_patchaosp7r35.patch 

```shell
patch -p1 < 0001_patchaosp7r35.patch
```

```
合入转码补丁，代码仓根目录下：aosp-on-qemu/binaryAndFiles/Exagear_version_20191101_AOSP7.0/Exagear_V100R002C00SPC010B003/exagear-a32a64-vm.tar.gz
```

```shell
tar zxvf exagear-a32a64-vm.tar.gz    
解压后将ExaGear ARM32-ARM64 for Android VM/android/android-7.0.0_r35.patch拷贝到$(AOSP_DIR)下
patch -p1 < android-7.0.0_r35.patch
在将ExaGear ARM32-ARM64 for Android VM/android/vendor拷贝到$(AOSP_DIR)下
```

##### 2.2.3.2.3 aosp7.0.0r35镜像编译

```shell
cd /home/$(AOSP_DIR)
make clean  
make -j64
```

编译成功后，在$(AOSP_DIR)/out/target/product/generic_arm64可以看到如下最新生成的4个如下文件：  

cache.img  ramdisk.img  system.img  userdata.img。

##### 2.2.3.2.4 system-images目录创建及img文件拷贝

将x86上编译所得的4个img(system.img、userdata.img、ramdisk.img、cache.img)文件拷贝到鲲鹏920编译环境上的$( ANDROID_SDK_LINUX_DIR)/system-images/android-24/default/arm64-v8a/目录下。

代码仓根目录下：aosp-on-qemu/patchForEmuMasterDev/source.properties拷贝到指定目录，$( ANDROID_SDK_LINUX_DIR)/system-images/android-24/default/arm64-v8a/。

#### 2.2.3.3构建aosp9.0.0_r10的system-images目录（支持鲲鹏916服务器）

编译Android系统源码，第2.2.1章节准备的编译环境X86，操作系统ubuntu16.04.0x。

##### 2.2.3.3.1 下载aosp源码与编译（android-9.0.0_r10）

在AOSP9.0SOURCE目录下，下载aosp源码，版本android-9.0.0_r10：

```shell
repo init -u https://android.googlesource.com/platform/manifest -b android-9.0.0_r10
repo sync
//拷贝仓中更目录下 aosp-on-qemu/binaryAndFiles/snapshot-aosp9_B630.xml 快照文件到.repo/manifests/目录下，对aosp9的所有git仓进行代码回退
cp snapshot-aosp9_B630.xml  /home/$(AOSP_DIR)/.repo/manifests/
cd /home/$(AOSP_DIR)
repo init -m snapshot-aosp9_B630.xml
repo sync -d -j32
```

合入补丁，代码仓根目录下：aosp-on-qemu/patchForAosp9.0.0r10/0001_ril_network_and_adb_forward.patch：

```shell
cd AOSP9.0SOURCE
patch -p1 < 0001_ril_network_and_adb_forward.patch
```

编译aosp：

```shell
source build/envsetup.sh
lunch aosp_arm64-eng
make clean
make -j32
```

##### 2.2.3.3.2 镜像文件修改及拷贝

在鲲鹏920的编译环境上的android-sdk-linux目录下创建目录 system-images/android-28/default/arm64-v8a。

将x86上编译所得的6个img(system-qemu.img、userdata.img、ramdisk.img、cache.img、vbmeta.img、vendor-qemu.img)、advancedFeatures.ini拷贝到以上目录。

代码仓根目录下：aosp-on-qemu/patchForEmuMasterDev/source.properties拷贝到指定目录$( ANDROID_SDK_LINUX_DIR)/system-images/android-28/default/arm64-v8a/ 。另外上文中编译出来的android内核文件kernel-ranchu也要拷贝到该目录下面。

source.properties文件还需要修改如下两处内容：

```
Pkg.Revision=7  ---> Pkg.Revision=9
AndroidVersion.ApiLevel=24 ---> AndroidVersion.ApiLevel=28
```

修改vendor-qemu.img、system-qemu.img两个文件名称：

```shell
mv system-qemu.img  system.img
mv vendor-qemu.img vendor.img
```
#### 2.2.3.4 构建aosp9.0.0_r10的system-images目录（支持鲲鹏920服务器）

编译Android系统源码，第2.2.1章节准备的编译环境X86，操作系统ubuntu16.04.0x。

##### 2.2.3.4.1 下载aosp源码与编译（android-9.0.0_r10）

在AOSP9.0SOURCE目录下，下载aosp源码，版本android-9.0.0_r10：

```shell
repo init -u https://android.googlesource.com/platform/manifest -b android-9.0.0_r10
repo sync
//拷贝仓中更目录下 aosp-on-qemu/binaryAndFiles/snapshot-aosp9_B630.xml 快照文件到.repo/manifests/目录下，对aosp9的所有git仓进行代码回退
cp snapshot-aosp9_B630.xml  /home/$(AOSP_DIR)/.repo/manifests/
cd /home/$(AOSP_DIR)
repo init -m snapshot-aosp9_B630.xml
repo sync -d -j32
```

合入ril_network_and_adb_forward补丁（目前只有0003一个补丁），代码仓根目录下：aosp-on-qemu/patchForAosp9.0.0r10/0001_ril_network_and_adb_forward.patch：

```shell
cd AOSP9.0SOURCE
patch -p1 < 0001_ril_network_and_adb_forward.patch
```

合入转码补丁，代码仓根目录下：aosp-on-qemu/binaryAndFiles/Exagear_version_20191111_AOSP9.0/Exager_V100R002C00SPC011B001/exagear-a32a64-vm.tar.gz

```shell
#解压exagear-a32a64-vm.tar.gz
tar -zxvf exagear-a32a64-vm.tar.gz
cp -r ExaGear ARM32-ARM64 for Android VM/android/android-9.0.0_r10.patch AOSP9.0SOURCE
cd AOSP9.0SOURCE
patch -p1 < android-9.0.0_r10.patch
# 在把vendor拷贝到AOSP9.0SOURCE下
cp -r ExaGear ARM32-ARM64 for Android VM/android/vendor AOSP9.0SOURCE
```

编译aosp：

```shell
source build/envsetup.sh
lunch aosp_arm64-eng
make clean
make -j32
```

##### 2.2.3.4.2 镜像文件修改及拷贝

在鲲鹏920的编译环境上的android-sdk-linux目录下创建目录 system-images/android-28/default/arm64-v8a。

将x86上编译所得的6个img(system-qemu.img、userdata.img、ramdisk.img、cache.img、vbmeta.img、vendor-qemu.img)、advancedFeatures.ini拷贝到以上目录。

代码仓根目录下：aosp-on-qemu/patchForEmuMasterDev/source.properties拷贝到指定目录$( ANDROID_SDK_LINUX_DIR)/system-images/android-28/default/arm64-v8a/ 。另外上文中编译出来的android内核文件kernel-ranchu也要拷贝到该目录下面。

source.properties文件还需要修改如下两处内容：

```
Pkg.Revision=7  ---> Pkg.Revision=9
AndroidVersion.ApiLevel=24 ---> AndroidVersion.ApiLevel=28
```

修改vendor-qemu.img、system-qemu.img两个文件名称：

```shell
mv system-qemu.img  system.img
mv vendor-qemu.img vendor.img
```

# 3    目标机鲲鹏916 or 920 QEMU环境准备和运行指导

将鲲鹏920编译环境上的$( ANDROID_SDK_LINUX_DIR)打包发到另外鲲鹏916/920运行环境上。

## 3.1  基于鲲鹏**<u>916</u>**上云手机运行环境准备pre

（1）固件版本，下载位置：https://support.huawei.com/enterprise/zh/taishan-servers/taishan-2280-pid-21941616/software

（2）系统安装请参见：[原生解决方案] 《基于鲲鹏916 GPU驱动环境搭建软件移植指导书》

文档社区链接：https://bbs.huaweicloud.com/forum/thread-23177-1-1.html



## 3.2  基于鲲鹏**<u>920</u>**上云手机运行环境准备pre

#### 3.2.1 固件版本

下载位置：https://support.huawei.com/enterprise/zh/taishan-servers/taishan-2280-v2-pid-23201901/software

#### 3.2.2 系统安装

请参见： [原生解决方案] 《基于鲲鹏920 GPU驱动环境搭建软件移植指导书》

文档社区链接：https://bbs.huaweicloud.com/forum/thread-23404-1-1.html

#### 3.2.3 鲲鹏920主机系统内核修改（for 高密场景）

````shell
物理机准备
一、物理机物理内存<=512GB
二、使用aosp-on-qemu/binaryFiles/kunpen920Firmware/目录下的固件升级服务器固件
   升级顺序cpld -> iBMC -> bios
   升级完成后，重新上电生效
三、BIOS参数配置
   Power Policy：BIOS > Advanced > Performance Config > Power Policy 默认为Efficiency模式，设置为Performance模式
   Smmu: BIOS > Advanced > MISC Config > Support Smmu 设置为Enabled
   DDR频率：BIOS > Advanced > Memory Config > Custom Refresh Rate 默认为32ms，设置为64ms
【注意】鲲鹏920服务器、AMD GPU WX5100、GPU专用的PCIE Riser卡。
【注意】鲲鹏920服务器上使用显卡外接电源线时，必须使用鲲鹏920 Riser卡配套的外接电源线，绝不能与鲲鹏916的混用，混用会烧坏GPU（12V和地线线序不一致）。

//安装依赖
apt install dpkg dpkg-dev libncurses5-dev libssl-dev libpciaccess0
//查看系统可获得的内核源码
apt search linux-source
//下载系统源码文件（下载内核版本为4.15.0）
apt install linux-source-4.15.0=4.15.0-88.88
//解压源码
cd /usr/src/linux-source-4.15.0
tar -xjvf linux-source-4.15.0
````

````shell
//合入内核vmalloc性能补丁
//补丁位置：aosp-on-qemu/patchForKunpeng920HostOSKernel/0001_mm_vmalloc.c_improve_vmap_allocation.patch
cp 0001_mm_vmalloc.c_improve_vmap_allocation.patch /usr/src/linux-source-4.15.0/linux-source-4.15.0
cd /usr/src/linux-source-4.15.0/linux-source-4.15.0
patch -p1 -m < 0001_mm_vmalloc.c_improve_vmap_allocation.patch

//合入GPU并发太多帧率周期性下降优化措施
//补丁位置：aosp-on-qemu/patchForKunpeng920HostOSKernel/0002_ttm_gpu.patch
cp 0002_ttm_gpu.patch /usr/src/linux-source-4.15.0/linux-source-4.15.0
cd /usr/src/linux-source-4.15.0/linux-source-4.15.0
patch -p1 -m < 0002_ttm_gpu.patch
````

#### 3.2.4 鲲鹏920主机系统内核安装编译

````shell
make menuconfig    //直接保存、退出，生成配置文件.config
make -j64               //编译
make modules            //编译模块
make modules_install    //安装模块
make install            //安装内核
cd /boot/grub   
sudo update-grub2       //更新启动项
reboot
````

#### 3.2.5 鲲鹏920主机侧参数调优

````shell
//修改进程最大文件打开数上限
vi /etc/security/limits.conf
//增加如下代码
root   soft    nofile          65535 
root   hard    nofile          65535 
*   soft    nofile          65535 
*   hard    nofile          65535
````

#### 3.2.6 鲲鹏920上qemu启动方式优化，numa绑定（启动脚本仅供参考）

````shell
//启动时NUMA绑核
numactl -N 0 -m 0 emulator -avd test_2 -no-window -cores 2 -writable-system -gpu host -qemu --enable-kvm -m 1024 -vnc  :2
如果要将AVD限定在某几个核上，则可以这样启动
numactl -C 0-3 -m 0 emulator -avd test_2 -no-window -cores 2 -writable-system -gpu host -qemu --enable-kvm -m 1024 -vnc  :2
//启动参考脚本在位置：aosp-on-qemu/refForStartQemuScripts/start_qemu.sh
````
#### 3.2.7 第三方工具使用

````shell
//adb使用（adb最大连接数为256）
adb工具位置：aosp-on-qemu/tool
该adb为arm侧服务器使用，将adb放到/usr/bin下面
//radeon工具使用（采集显卡信息）
radeon工具位置：aosp-on-qemu/tool
将工具放到服务器的任意目录下，之后即可在该目录下使用./radeontop -b 1 -c（数值代表GPU的ID）来采集显卡的信息
//umr使用（采集pcie带宽信息）
umr工具位置aosp-on-qemu/tool
将工具放到服务器的任意目录下，之后即可在该目录下使用./umr -i 1 --top （数值代表GPU的ID）来采集带宽
kdump的安装使用和转储日志操作：详情参考aosp-on-qemu/docForUserlnstructionManual/使用kdump与转储日志操作文档
scrcpy投屏工具的安装使用：详情参考aosp-on-qemu/docForUserlnstructionManual/Scrcpy投屏工具安装使用文档
````

## 3.3  基于鲲鹏服务器的android模拟器环境搭建指导

请参见：

[原生解决方案] 《基于鲲鹏芯片的Android模拟器环境搭建指导书》文档社区链接：https://bbs.huaweicloud.com/forum/thread-23014-1-1.html

针对配置了GPU的环境，运行Android模拟器需要依赖GPU驱动环境，请在运行前搭建好GPU驱动环境，鲲鹏920搭建步骤具体请参见《基于鲲鹏920的GPU驱动环境搭建移植指导书》（https://bbs.huaweicloud.com/forum/thread-23404-1-1.html），鲲鹏916搭建步骤具体请参见《基于鲲鹏916的GPU驱动环境搭建软件移植指导书》(https://bbs.huaweicloud.com/forum/thread-23177-1-1.html)

## 3.4  基于鲲鹏服务器的 os与驱动](https://bbs.huaweicloud.com/forum/forum-927-1.html) 优化, 请参见链接：

- 鲲鹏性能优化十板斧(一）——鲲鹏处理器NUMA简介与性能调优五步法 

  https://bbs.huaweicloud.com/forum/thread-27442-1-1.html 

- 鲲鹏性能优化十板斧（二）——CPU与内存子系统性能调优 

  https://bbs.huaweicloud.com/forum/thread-27525-1-1.html 

- 鲲鹏性能优化十板斧（三）——网络子系统性能调优 

  https://bbs.huaweicloud.com/forum/thread-27815-1-1.html 

- 鲲鹏性能优化十板斧（四）——磁盘IO子系统性能调优

   https://bbs.huaweicloud.com/forum/thread-27816-1-1.html

- 鲲鹏性能优化十板斧（五）——应用程序性能调优

   https://bbs.huaweicloud.com/forum/thread-28191-1-1.html

   ## 4   qemu容器方案安装环境指导

####       4.1 系统安装

````shell
【系统iso下载链接】https://cdimage.debian.org/debian-cd/10.3.0/arm64/iso-dvd/debian-10.3.0-arm64-DVD-1.iso
【安装指导】
【注意事项】安装操作系统的最后一步“选择软件列表”时，“空格”选用“openssh软件”。
````

####       4.2 鲲鹏920上依赖库安装和基础组件安装

````shell
//Debian源可以配置如下：
deb http://mirrors.163.com/debian/ buster main non-free contrib
deb http://mirrors.163.com/debian/ buster-updates main non-free contrib
deb http://mirrors.163.com/debian/ buster-backports main non-free contrib
deb http://mirrors.163.com/debian-security/ buster/updates main non-free contrib

deb-src http://mirrors.163.com/debian/ buster main non-free contrib
deb-src http://mirrors.163.com/debian/ buster-updates main non-free contrib
deb-src http://mirrors.163.com/debian/ buster-backports main non-free contrib
deb-src http://mirrors.163.com/debian-security/ buster/updates main non-free contrib
````

````shell
apt update
apt install docker.io openjdk-11-jdk
````

####       4.3 建立工程目录

````shell
//脚本在/aosp-on-qemu/docker-qemu 
//docker镜像包内部onebox链接：https://onebox.huawei.com/p/91be8850d937f376742bf4fba97b4cc2
//创建工程目录
mkdir /home/Qemu
在目录下存放android-sdk-linux kernel-ranchu launch_18.04wc_on_shutdownads.sh ubuntu18.04cs_wc_on.tar xorg.conf
// 注册docker镜像
docker load  -i ubuntu18.04cs_wc_on.tar
docker tag xxxx ubuntu18.04cs_wc_on:latest
// 修改脚本
WORK_PATH=/home/Qemu
创建一个存放日志的目录
logDir=xxxx
在工程目录下执行launch_18.04wc_on_shutdownads.sh
// 参考文档在/aosp-on-qemu/docker-qemu/qemu启动操作指南---debian系统.docx
````

