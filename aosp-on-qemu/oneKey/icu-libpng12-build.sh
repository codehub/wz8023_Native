# 操作环境为：编译环境鲲鹏916，操作系统ubuntu16.04.0x
# libicui18n.so  libicui18n.so.52  libicui18n.so.52.1
LOCAL_PATH=`pwd`
target_dir=android-sdk-linux
BUILD_DIR=build-dir
mkdir $BUILD_DIR
cd $BUILD_DIR
wget http://archive.ubuntu.com/ubuntu/pool/main/i/icu/icu_52.1.orig.tar.gz
tar -xvf icu_52.1.orig.tar.gz
cd icu/source
./configure
make
    
# libpng12.so.0
wget http://archive.ubuntu.com/ubuntu/pool/main/libp/libpng/libpng_1.2.50.orig.tar.xz
tar -xvf libpng_1.2.50.orig.tar.xz
cd libpng-1.2.50
./configure --build=arm-unknown-linux
make

#cd $LOCAL_PATH
#mkdir -p $target_dir/tools/lib64/qt/lib
#cp -r $BUILD_DIR/icu/source/lib/*  $target_dir/tools/lib64/qt/lib/
#mkdir -p $target_dir/tools/lib64/
#cp -r $BUILD_DIR/icu/source/libpng-1.2.50/.libs/* $target_dir/tools/lib64/
