[TOC]

#            内源云手机Robox解决方案构建指导书

```shell
【"注意事项"】当前提供的构建指导书版本中，所有操作需要在"root账号下操作"，在后续的版本中会逐步迁移到普通用户。
```

# 一、x86上安卓镜像编译构建

```
【说明1】发布的版本中提供已经编译好的安卓镜像(可能会带时间戳，请根据实际情况来操作)，可直接使用。若使用版本中提供的镜像包，则跳过这一章节，直接从第二章开始。
【说明2】本文档中为方便理解，所有路径均采用了具体的路径，实际操作过程中，可根据实际情况，使用自己的路径
```

## 1.1 x86基础编译环境准备

```shell
【说明】x86上的编译构建环境，可以在ubuntu18.04.x上进行，也可以在ubuntu16.04.x上进行，本文档中以ubuntu18.04.1为例

【操作系统下载链接180401】http://old-releases.ubuntu.com/releases/18.04.1/ubuntu-18.04.1-server-amd64.iso
//google官网参考链接：https://source.android.com/source/initializing.html
//去掉/etc/apt/source.list中的deb-src的注释
# sed -i "s/# deb-src/ deb-src/g" /etc/apt/sources.list 

# apt update
# apt install -y openjdk-8-jdk
# apt install -y libx11-dev libreadline6-dev libgl1-mesa-dev g++-multilib 
# apt install -y git flex bison gperf build-essential libncurses5-dev 
# apt install -y tofrodos python-markdown libxml2-utils xsltproc zlib1g-dev 
# apt install -y dpkg-dev libsdl1.2-dev
# apt install -y git-core gnupg flex bison gperf build-essential  
# apt install -y zip curl zlib1g-dev gcc-multilib g++-multilib 
# apt install -y libc6-dev 
# apt install -y lib32ncurses5-dev x11proto-core-dev libx11-dev 
# apt install -y libgl1-mesa-dev libxml2-utils xsltproc unzip m4
# apt install -y lib32z-dev ccache
# apt install -y bc python flex bison gperf libsdl-dev build-essential zip curl
```

## 1.2 robox源码下载

```shell
【注意】下载anbox主线代码，commit为 3ed2e6d5c360d57b6aa61386e279adf3ff155ded
# cd /home/
# git clone https://github.com/anbox/anbox.git
# cd anbox/
# git reset --hard 3ed2e6d5c360d57b6aa61386e279adf3ff155ded
//显示信息如下信息回退成功
//HEAD is now at 3ed2e6d Merge pull request #1258 from stephengroat/dockerfile

//x86机器上，下载后目录为:/home/anbox
【注意】如源码下载失败，提示验证，通过以下跳过验证
# export GIT_SSL_NO_VERIFY=1
```

## 1.3 安卓源码下载，补丁合入

### 1.3.1 下载安卓源码

```shell
# mkdir ~/bin
# PATH=~/bin:$PATH
//下载 Repo 工具，并确保它可执行：
# curl https://storage.googleapis.com/git-repo-downloads/repo > ~/bin/repo
# chmod a+x ~/bin/repo
//安卓源码下载
# mkdir -p /home/android
# cd /home/android/
# repo init -u https://github.com/anbox/platform_manifests.git -b anbox
# repo sync -j64  //同步代码，下载后代码目录总计100GB左右

//使用仓中repo仓中快照xml文件，回退repo仓代码，快照位置aosp-on-anbox/binaryFiles/snapshot20191206.xml
# cp snapshot20191206.xml /home/android/.repo/manifests/
# repo init -m snapshot20191206.xml
# repo sync -d -j64   //进行代码回退
```

```shell
//安卓源码下anbox目录替换
//删除/home/android/vendor/目录下的anbox目录
# rm -rf /home/android/vendor/anbox
//将下载的anbox代码，拷贝到指定目录
# cp -r /home/anbox /home/android/vendor/
```

### 1.3.2 合入adb连接相关补丁

```shell
//(1)修改adb转发路径，同时连接台数限制
# cp 0002_android_adb.diff /home/android/system/core/
# cd /home/android/system/core/
# patch -p1 < 0002_android_adb.diff

//(2)修改rootfs的挂载属性
# cp 0004_android_mount_rw.diff /home/android/system/core/
# cd /home/android/system/core/
# patch -p1 < 0004_android_mount_rw.diff
```

### 1.3.3 合入获取FPS数据补丁

```shell
//将0005补丁拷贝到安卓源码如下目录
# cp 0005_android_frameworks_native_printFPS.diff  /home/android/frameworks/native/
# cd /home/android/frameworks/native/
# patch -p1 < 0005_android_frameworks_native_printFPS.diff

//使用说明
  (1)使用新编译的安卓镜像启动robox实例；
  (2)adb shell登陆到robox容器实例中，或者使用docker exec登录到robox容器实例中
  (3)在安卓系统内执行 
     setprop debug.egl.call_freq true
  (4)在安卓系统内，通过logcat命令查看帧率显示
     logcat | grep libEGL
```

### 1.3.4 合入arm32toarm64转码补丁

```shell
【说明】exagear补丁分为安卓源码补丁，和物理机kernel补丁
//合入exagear转码补丁
//将提供的转码包exagear-a32a64-docker.tar.gz，解压到/home/目录下，并将解压后的文件重命名成exagear，以方便后续操作
# cd /home/
# tar zxvf exagear-a32a64-docker.tar.gz
# mv ExaGear\ ARM32-ARM64\ for\ Android\ Docker/ exagear
# cd /home/exagear/
# ls
  android      //安卓源码转码补丁
  translator   //物理主机上转码二进制可执行文件
  kernel       //物理机内核的转码补丁
# cd android/
# ls
  android-7.1.1_r13.patch     //安卓系统的转码补丁
  vendor                      //转码所需，拷贝到安卓源码根目录下

//安卓系统所需合入的源码补丁，为/home/exagear/android/android-7.1.1_r13.patch
# cd /home/android/   //进入到下载的安卓源码更目录
//合入补丁
# patch -p1 < /home/exagear/android/android-7.1.1_r13.patch 

//将压缩包中vendor目录拷贝到aosp源码下，合并原来的vendor目录
//说明：/home/android/目录下已经有vender/anbox/目录，拷贝转码中的vender目录过来后，不会直接覆盖，而是会将两个vender目录合并
# cp -r /home/exagear/android/* /home/android/
# ls /home/android/vendor/
  anbox
  huawei
//上面的huawei目录包含转码相关

//合入0001号补丁，使得exagear编译进aosp镜像
# cp 0001_android_anbox_mk.diff /home/android/vendor/anbox/
# cd /home/android/vendor/anbox/
# patch -p1 < 0001_android_anbox_mk.diff
```

### 1.3.5 合入ip特性补丁

```shell
//将0013_generate_ipconfig_feature.patch拷贝到安卓源码如下目录
# cp 0013_generate_ipconfig_feature.patch /home/android/vendor/anbox/
# cd /home/android/vendor/anbox/
# patch -p2 < 0013_generate_ipconfig_feature.patch
```

### 1.3.6 合入多实例补丁

```shell
//将0014_multi_instances_support.patch拷贝到安卓源码如下目录
# cp 0014_multi_instances_support.patch /home/android/vendor/anbox/
# cd /home/android/vendor/anbox/
# patch -p2 < 0014_multi_instances_support.patch
```

### 1.3.7 合入Zygtote32和编译选项支持补丁

```shell
//将0015_Zygtote32_and_other_compile_options_support.patch拷贝到安卓源码如下目录
# cp 0015_Zygtote32_and_other_compile_options_support.patch /home/android/vendor/anbox/
# cd /home/android/vendor/anbox/
# patch -p2 < 0015_Zygtote32_and_other_compile_options_support.patch
```

### 1.3.8 合入横屏及录屏补丁

```shell
//将0006补丁拷贝到安卓源码如下目录
# cp 0006_android_vendor_anbox_products_modifyResolution.diff /home/android/vendor/anbox/
# cd /home/android/vendor/anbox/
# patch -p2 < 0006_android_vendor_anbox_products_modifyResolution.diff
```

### 1.3.9 opengl2.0部分游戏运行崩溃修复补丁

```shell
//将0007补丁拷贝到安卓源码如下目录
# cp 0007_android_base_gl2.0_modify.diff /home/android/frameworks/base
# cd /home/android/frameworks/base
# patch -p1 < 0007_android_base_gl2.0_modify.diff
```

### 1.3.10 合入aosp部分的vmalloc性能补丁
```shell
//将patches/vmalloc_performance文件夹中的0010_android_frameworks_base_vmalloc.diff补丁拷贝到aosp源码如下目录
# cp 0010_android_frameworks_base_vmalloc.diff /home/android/frameworks/base
# cd /home/android/frameworks/base
# patch -p4 < 0010_android_frameworks_base_vmalloc.diff
```

### 1.3.11 按home键/或者游戏退出回到home界面没有图标问题修复补丁

```shell
//将 0009_vendor_anbox_launcher.diff补丁拷贝到vendor/anbox目录
# cp 0009_vendor_anbox_launcher.diff /home/android/vendor/anbox
# cd /home/android/vendor/anbox
# patch -p1 < 0009_vendor_anbox_launcher.diff
```

### 1.3.12 合入屏蔽音频ALSA的错误打印补丁

```
//补丁位置：aosp-on-anbox/patches/0008_android_audio_ALSA_no_log.diff
# cp 0008_android_audio_ALSA_no_log.diff /home/android/vendor/anbox/
# cd /home/android/vendor/anbox/
# patch -p1 < 0008_android_audio_ALSA_no_log.diff
```

### 1.3.13 编译安卓源码

```shell
//编译安卓镜像
# cd /home/android/
# source build/envsetup.sh   //配置编译环境
# lunch anbox_arm64-userdebug   //选择编译环境
# export JACK_EXTRA_CURL_OPTIONS=-k
# export LC_ALL=C
# make -j48      //编译
//在/home/android/out/target/product/arm64/目录下会生成需要的ramdisk.img和system.img 安卓镜像文件
```

### 1.3.14 生成安卓镜像

```shell
//合成robox所需的docker容器镜像文件：android.img文件
# cd /home/android/vendor/anbox/
# scripts/create-package.sh /home/android/out/target/product/arm64/ramdisk.img /home/android/out/target/product/arm64/system.img
//将在当前目录，即/home/android/vendor/anbox/下合成android.img镜像
//将该android.img文件拷贝到鲲鹏920服务器的/home目录下
```

# 二、鲲鹏920上robox运行环境准备(先在BIOS中将Power Pilicy设置为Performance模式)

## 2.1 鲲鹏920上操作系统安装

```shell
1)主机固件版本和安装
   升级顺序CPLD >  iBMC  >  BIOS， CPLD和BIOS 需要重新上电生效；
   固件包位于binaryFiles/kunpen920Firmware目录下
2)硬件要求和注意事项
   【BIOS设置1】BIOS > Power Policy > 默认为Efficiency模式，设置为Performance模式
   【BIOS设置2】BIOS > Advanced > Memory Config > Custom Refresh Rate > 默认为32ms，设置为64ms

   【注意】鲲鹏920服务器、AMD GPU WX5100、GPU专用的PCIE Riser卡。
   【注意】鲲鹏920服务器上使用显卡外接电源线时，必须使用鲲鹏920 Riser卡配套的外接电源线，绝不能与鲲鹏916的混用，混用会烧坏GPU（12V和地线线序不一致）。
3)操作系统安装（ubuntu18.04.1）
   【系统iso下载链接】http://old-releases.ubuntu.com/releases/18.04.1/ubuntu-18.04.1-server-arm64.iso
   【指导链接】https://support.huawei.com/enterprise/zh/doc/EDOC1100100619?idPath=7919749%7C9856522%7C9856629%7C23201901
   【注意事项】安装操作系统的最后一步“选择软件列表”时，“空格”选用“openssh软件”。
   【本地工具建议】推荐使用MobaXterm作为远程工具。
```

## 2.2 鲲鹏920上依赖库安装和基础组件安装

```shell
//去掉/etc/apt/source.list中的deb-src的的注释
# sed -i "s/# deb-src/ deb-src/g" /etc/apt/sources.list 
# apt update
//系统基础依赖库安装，【提示】如果安装过程中有获取deb包失败的情况，请根据提示中的网址，手动下载安装，然后重新安装没成功的包
# apt install dpkg libncurses5-dev libncursesw5-dev libssl-dev cmake cmake-data debhelper dbus google-mock libboost-dev libboost-filesystem-dev libboost-log-dev libboost-iostreams-dev libboost-program-options-dev libboost-system-dev libboost-test-dev libboost-thread-dev libcap-dev libsystemd-dev libdbus-1-dev libegl1-mesa-dev  libgles2-mesa-dev libglib2.0-dev libglm-dev libgtest-dev liblxc1 libproperties-cpp-dev libprotobuf-dev libsdl2-dev libsdl2-image-dev lxc-dev pkg-config protobuf-compiler libboost-filesystem1.62.0 libboost-system1.62.0 docker.io dkms libboost-iostreams1.62.0
# apt install build-essential
# apt install mesa-common-dev

//下载位置https://launchpad.net/ubuntu/zesty/arm64/libprocess-cpp3/3.0.1-0ubuntu5，Linux中下载：wget http://launchpadlibrarian.net/291649807/libprocess-cpp3_3.0.1-0ubuntu5_arm64.deb
# dpkg -i libprocess-cpp3_3.0.1-0ubuntu5_arm64.deb

//依赖库，缺少的libdbus-cpp5_5.0.0+16.10.20160809-0ubuntu2_arm64.deb
//A:从https://launchpad.net/ubuntu/zesty/arm64/libdbus-cpp5/5.0.0+16.10.20160809-0ubuntu2官方下载，Linux中下载：wget http://launchpadlibrarian.net/291804794/libdbus-cpp5_5.0.0+16.10.20160809-0ubuntu2_arm64.deb
# dpkg -i libdbus-cpp5_5.0.0+16.10.20160809-0ubuntu2_arm64.deb

//依赖库，缺少的libdbus-cpp-dev_5.0.0+16.10.20160809-0ubuntu2_arm64.deb
//A：从https://launchpad.net/ubuntu/bionic/arm64/libdbus-cpp-dev/5.0.0+16.10.20160809-0ubuntu2官方下载，Linux下载：wget http://launchpadlibrarian.net/291804785/libdbus-cpp-dev_5.0.0+16.10.20160809-0ubuntu2_arm64.deb
# dpkg -i libdbus-cpp-dev_5.0.0+16.10.20160809-0ubuntu2_arm64.deb
```

```shell
//配置docker storage driver
# docker info  //查看docker基本信息
  Containers: 0
   Running: 0
   Paused: 0
   Stopped: 0
  Images: 0
  Server Version: 18.09.7
  Storage Driver: overlay2
   Backing Filesystem: extfs
   Supports d_type: true
   Native Overlay Diff: true
  Logging Driver: json-file
  Cgroup Driver: cgroupfs
  ......
//修改“Storage Driver”为overlay，若已经为overlay或者overlay2，则不需要修改
//当“Storage Driver”为aufs
# vim /etc/default/docker
  //在脚本中添加如下
  DOCKER_OPTS= -s overlay
//重启docker
# /etc/init.d/docker restart
```

```shell
//安装远程桌面
# apt install xfce4  xfce4-* xrdp
//无硬件GPU环境配置的情况如下
# cd /home/ubuntu
# vi .xsession  //在文本中添加如下内容
  xfce4-session
//保存后确认
# cat .xsession
  xfce4-session
//重启xrdp远程桌面
# /etc/init.d/xrdp restart
//此后，可以通过window的远程桌面访问到该鲲鹏920服务器图形桌面，使用root账号登陆
```

## 2.3 鲲鹏920上内核源码编译

### 2.3.1  合入exagear转码补丁

```shell
# mkdir -p /home/compiler

//物理机内核源码下载地址：https://launchpad.net/ubuntu/+source/linux/4.15.0-65.74
//下载其中的linux_4.15.0.orig.tar.gz   linux_4.15.0-65.74.diff.gz   linux_4.15.0-65.74.dsc三个源码文件，放到/home/compiler/目录。
// Linux上下载：
// wget https://launchpad.net/ubuntu/+archive/primary/+sourcefiles/linux/4.15.0-65.74/linux_4.15.0.orig.tar.gz --no-check-certificate
// wget https://launchpad.net/ubuntu/+archive/primary/+sourcefiles/linux/4.15.0-65.74/linux_4.15.0-65.74.diff.gz --no-check-certificate
// wget https://launchpad.net/ubuntu/+archive/primary/+sourcefiles/linux/4.15.0-65.74/linux_4.15.0-65.74.dsc --no-check-certificate
# cd /home/compiler/
# ls
  linux_4.15.0-65.74.diff.gz  linux_4.15.0-65.74.dsc  linux_4.15.0.orig.tar.gz
# dpkg-source -x linux_4.15.0-65.74.dsc
//将在当前目录下生成源码目录linux-4.15.0

//将提供的压缩包exagear-a32a64-docker.tar.gz，解压到/home/目录下，并将解压后的文件重命名成exagear
# cd /home/
# tar zxvf exagear-a32a64-docker.tar.gz
# mv ExaGear\ ARM32-ARM64\ for\ Android\ Docker/ exagear  //重命名目的：方便
# cd /home/exagear
# ls
  android      //安卓源码转码补丁
  translator   //物理主机上转码二进制可执行文件
  kernel       //物理机内核的转码补丁

//拷贝转码补丁到内核源码目录
# cp /home/exagear/kernel/ubuntu-4.15.0-65.74.patch  /home/compiler/linux-4.15.0/
# cd /home/compiler/linux-4.15.0/
# patch -p1 < ubuntu-4.15.0-65.74.patch
```

### 2.3.2 合入pmu支持补丁

```shell
//补丁位置：aosp-on-anbox/patches/0016-drivers-perf-hisi-update-the-sccl_id-ccl_id-when-MT-.patch
# cp 0016-drivers-perf-hisi-update-the-sccl_id-ccl_id-when-MT-.patch /home/compiler/linux-4.15.0/
# cd /home/compiler/linux-4.15.0/
# patch -p1 < 0016-drivers-perf-hisi-update-the-sccl_id-ccl_id-when-MT-.patch
```


### 2.3.3 合入内核vmalloc性能补丁

```shell
//补丁位置：aosp-on-anbox/patches/vmalloc_performance/0011-mm-vmalloc.c-improve-vmap-allocation-bytedance.patch
# cp 0011-mm-vmalloc.c-improve-vmap-allocation.patch /home/compiler/linux-4.15.0/
# cd /home/compiler/linux-4.15.0/
# patch -p1 < 0011-mm-vmalloc.c-improve-vmap-allocation.patch
```


### 2.3.4 编译安装内核

```shell
# make menuconfig    //直接保存、退出，生成配置文件.config
# vim .config        //确认以下配置项，没有或不一致时请手动更改或添加
  CONFIG_BINFMT_MISC=y
  CONFIG_EXAGEAR_BT=y
  CONFIG_CHECKPOINT_RESTORE=y
  CONFIG_PROC_CHILDREN=y
  CONFIG_VFAT_FS=y
  CONFIG_INPUT_UINPUT=y
  HISI_PMU=y
# make -j64               //编译
# make modules_install    //安装模块
# make install            //安装内核
# cd /boot/grub   
# sudo update-grub2       //更新启动项
//【注意】修改内核启动选项
//在/boot/grub/grub.cfg文件中，内核启动参数带上iommu.strict=0 pci=pcie_bus_perf
//示例：linux /boot/vmlinuz-4.15.0324+ root=/dev/mapper/ubuntu--73--vg-root ro  iommu.strict=1 pci=pcie_bus_perf
# reboot
```

### 2.3.5 新内核启用后转码注册

```shell
//物理机重启后，转码注册使能
//挂载binfmt_misc文件系统，默认已挂载，如未挂载，请手动执行：
# mount -t binfmt_misc none /proc/sys/fs/binfmt_misc

//注册exagear转码规则，注意以下多处存放和使用ubt_a32a64转码二进制的“/opt/exagear/ubt_a32a64”路径信息要保持一致
# mkdir -p /opt/exagear
# cp /home/exagear/translator/ubt_a32a64  /opt/exagear/
# cd /opt/exagear  //存放转码的二进制文件目录，echo注册时，注意/opt/exagear/ubt_a32a64
# chmod +x ubt_a32a64   //避免注册permission denied 
//【注意】每次服务器重启后，都需要手动重新注册转码规则，切记！！
# echo ":ubt_a32a64:M::\x7fELF\x01\x01\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x02\x00\x28\x00:\xff\xff\xff\xff\xff\xff\xff\x00\x00\x00\x00\x00\x00\x00\x00\x00\xfe\xff\xff\xff:/opt/exagear/ubt_a32a64:POCF" > /proc/sys/fs/binfmt_misc/register

//查看exagear规则是否注册成功，确保/opt/exagear/ubt_a32a64路径信息一致
# cat /proc/sys/fs/binfmt_misc/ubt_a32a64
  enabled
  interpreter /opt/exagear/ubt_a32a64
  flags: POCF
  offset 0
  magic 7f454c4601010100000000000000000002002800
  mask ffffffffffffff000000000000000000feffffff

//以下可选操作，检查确认物理内核的arm32转arm64程序是否运行正常，其中mpro32为一个arm32位的二进制可执行文件，可以是任意arm32位的二进制可行性文件
# echo 1 > /proc/sys/fs/binfmt_misc/status     //转码默认启动状态，echo 1为启动，echo 0为关闭
# ./mpro32
  mprotect successhellow workd

//以下是echo 0为关闭时，测试程序
# echo 0 > /proc/sys/fs/binfmt_misc/status
# ./ubt_a32a64 mpro32
  mprotect successhellow workd
```

## 2.4 鲲鹏920上binder.ko和ashmem.ko模块编译安装

```shell
//下载内核源码，供内核模块编译
# apt search linux-source
# apt install linux-source-4.15.0

//参考社区链接https://github.com/anbox/anbox-modules
# cd /home
# git clone https://github.com/anbox/anbox-modules.git
# cd anbox-modules
# git reset --hard 816dd4d6e702cf77a44cfe208659af6c39e02b57
//确保回退到如下commit
//Merge: 27fd47e 85d3854
//Author: Simon Fels <morphis@gravedo.de>
//Date:   Sat Jul 13 11:32:21 2019 +0200
//    Merge pull request #19 from 86423355844265459587182778/master
//    Fix compilation on kernels >= 5.1

//更新ashmem版本
//将patches/0012_ashmem_version_update.diff 补丁拷贝到ashmem源码目录
# cp 0012_ashmem_version_update.diff  /home/anbox-modules/ashmem
# cd /home/anbox-modules/ashmem
//通过补丁形式升级ashmem版本
# patch -p1 < 0012_ashmem_version_update.diff

# cd /home/anbox-modules
# cp anbox.conf /etc/modules-load.d/
# cp 99-anbox.rules /lib/udev/rules.d/
# cp -rT ashmem /usr/src/anbox-ashmem-1
# cp -rT binder /usr/src/anbox-binder-1

//使用dkms进行编译和安装
# dkms install anbox-ashmem/1
# dkms install anbox-binder/1

//将ko模块插入内核，注意，binder_linux模块需要带参数
//【注意】每次服务器重启后，需要将binder_linux模块移除之后，再重新安装
# modprobe ashmem_linux
# modprobe binder_linux num_devices=254
# lsmod | grep -e ashmem_linux -e binder_linux

//若ashmem和binder的属性权限不是下列命令显示的，请用chmod添加
# ls -alh /dev/binder* /dev/ashmem
crwxrwxrwx 1 root root  10, 55 Oct 22 10:47 /dev/ashmem
crwxrwxrwx 1 root root 511,  0 Oct 22 10:47 /dev/binder0
crwxrwxrwx 1 root root 511,  0 Oct 22 10:47 /dev/binder1
...
```

## 2.5 鲲鹏920上导入安卓镜像

```
【说明】使用第一章节中编译好的android镜像，或者使用发布包中自带的安卓镜像(自带安卓镜像的名称可能带有时间戳等信息）实际操作过程中，请以实际名称为准。本文档中以不带时间戳的名称为例进行操作
```

```shell
//android.img文件导入，假设安卓镜像已经上传到鲲鹏920的/home目录下
# cd /home
# mount /home/android.img /mnt       
# cd /mnt
//打包，并导入docker镜像仓库中，其中“robox_with_exagear”为标签名，实际操作中可以根据实际情况自行定义
# tar --numeric-owner -cf- . | docker import - android:robox_with_exagear    
# docker images   //查看系统中镜像
REPOSITORY          TAG                 IMAGE ID            CREATED             SIZE
android             robox_with_exagear    xxxxxxxx        x seconds ago         xxxMB
```

## 2.6 鲲鹏920上robox源码编译

```shell
【注意】下载anbox主线代码，commit为 3ed2e6d5c360d57b6aa61386e279adf3ff155ded
# cd /home/
# git clone https://github.com/anbox/anbox.git
# cd anbox/
# git reset --hard 3ed2e6d5c360d57b6aa61386e279adf3ff155ded
//显示信息如下信息回退成功
//HEAD is now at 3ed2e6d Merge pull request #1258 from stephengroat/dockerfile
【注意】如源码下载失败，提示验证，通过以下跳过验证
# export GIT_SSL_NO_VERIFY=1

//下载后robox目录为:/home/anbox，重命名为robox
# mv /home/anbox /home/robox
//合入如下0013 0014 0015三个补丁
# cp 0013_generate_ipconfig_feature.patch /home/robox/
# cp 0014_multi_instances_support.patch /home/robox/
# cp 0015_Zygtote32_and_other_compile_options_support.patch /home/robox/
# cd /home/robox/
# patch -p2 < 0013_generate_ipconfig_feature.patch
# patch -p2 < 0014_multi_instances_support.patch
# patch -p2 < 0015_Zygtote32_and_other_compile_options_support.patch
# cd /home/robox
# mkdir build
# cd /home/robox/build
# cmake ..   //配置编译，在/home/robox/build目录执行命令；另外，cmake时加上参数-DCMAKE_BUILD_TYPE=Debug，可以编译成debug版本，session-manager等报错的时候，会有堆栈等信息打印出来
//在/usr/include/glm/gtx/transform.hpp文件中添加以下内容
  #define GLM_ENABLE_EXPERIMENTAL

# make -j64  //编译
# make install  //安装
```

## 2.7 鲲鹏920上robox下显卡使能

```shell
//安装xfce4桌面和相关工具
# apt install -y xfce4  mesa-utils x11vnc vainfo
//修改xorg.conf配置文件
# cd /etc/X11
# touch xorg.conf
//在xorg.conf文件中添加如下内容，其中BusID项需要根据机器中显卡pci号修改。【注意】BusID都是用" : "分隔
Section "ServerFlags"
        Option "DontVTSwitch" "on"
        Option "AutoAddDevices" "off"
        Option "AutoEnableDevices" "off"
        Option "AutoAddGPU" "off"
        Option "AutoBindGPU" "off"
EndSection

Section "Device"
        Identifier "AMD"
        Driver "amdgpu"
        BusID "pci:01:00:00"
EndSection

Section "Monitor"
        Identifier "monitor0"
        Modeline "1280x720"   74.50  1280 1344 1472 1664  720 723 728 748 -hsync +vsync
        Option "enable" "true"
EndSection

Section "Screen"
        Identifier "screen0"
        Device "AMD"
        Monitor "monitor0"
        DefaultDepth 24
        SubSection "Display"
                Depth 24
                Modes "1280x720"
        EndSubSection
EndSection

//若GPU是WX5100，则BusID可用如下命令查询
# lspci | grep 5100
01:00.0 VGA compatible controller: Advanced Micro Devices, Inc. [AMD/ATI] Ellesmere [Radeon Pro WX 5100]
//将01:00.00填入xorg.conf文件中的字段：BusID "pci:01:00:00"
//Modeline "1280x720" 字段需要大于等于安卓系统的分辨率

//设置显卡性能模式，【注意】每次重启物理服务器，都需要设置一次
# echo high > /sys/class/drm/"card1"/device/power_dpm_force_performance_level
//其中card1可用以下命令查询：
# ls /sys/bus/pci/devices/0000\:01\:00.0/drm
  card1 controlD65 renderD128
#ls /sys/bus/pci/devices/0000\:89\:00.0/drm
  card6 controlD70 renderD133
```

```shell
//编译安装mesa-19.0.8版本
重装mesa方法如下：
1. 下载mesa代码
	# cd /home/
	# git clone https://anongit.freedesktop.org/git/mesa/mesa.git
2. 进入mesa代码目录，切换到19.0.8分支
    # cd /home/mesa
    # git checkout mesa-19.0.8
3. 下载编译依赖包
    # apt build-dep mesa
    # apt install libomxil-bellagio-dev libva-dev  llvm-7  llvm-7-dev python-mako
4. 执行autogen.sh生成Makefile
	# ./autogen.sh  --enable-texture-float --with-gallium-drivers=radeonsi,swrast --with-dri-drivers=radeon,swrast --with-platforms=drm,x11 --enable-glx-tls --enable-shared-glapi --enable-dri3 --enable-lmsensors  --enable-gbm --enable-xa --enable-osmesa  --enable-vdpau --enable-nine --enable-omx-bellagio --enable-va --with-llvm-prefix=/usr/lib/llvm-7 --enable-llvm --target=aarch64-linux-gnu CFLAGS="-fsigned-char -O2" CPPFLAGS="-fsigned-char -O2" CXXFLAGS="-fsigned-char -O2" --enable-autotools
5. 编译并安装
	# make -j32 && make install

6. 修改ldconfig更改库链接顺序
    # vim /etc/ld.so.conf
      添加 /usr/local/lib 到文件首内容，修改后内容如下
    # cat /etc/ld.so.conf
    /usr/local/lib
    include /etc/ld.so.conf.d/*.conf
    # ldconfig
```

## 2.8 支持WX 9100显卡的radeon工具安装
```shell
radeon工具下载地址： https://github.com/clbr/radeontop

下载完成后，将该工具传到使用机上。
//安装软件依赖
# apt install -y libpciaccess
//进入软件目录后，使用make进行编译
# make
//之后即可在该目录下使用./radeontop来采集WX 9100显卡的信息

```
```shell
umr工具下载地址： https://github.com/freedesktop/amd-umr
编译安装，请参见开源仓上说明
```

## 2.9 鲲鹏920上系统优化策略

```shell
1、进程参数优化
# vim /etc/sysctl.conf
    // 添加以下配置
    // root用户进程ID上限
    kernel.pid_max = 4119481
    // 物理内存支持的最大进程数
    kernel.threads-max = 4119481
// 单个用户允许的最大进程数上限，默认为threads-max的一半
# ulimit –u 2059740
2、设置最大文件打开数
# vim /etc/security/limits.conf
    //修改如下4项
    * soft core 0
    * hard core 0
    root soft nofile 165535
    root hard nofile 165535
3、设置用户可创建的instances上限
    //每次系统重启，都需要重新设置
    # sysctl -w fs.inotify.max_user_instances=81920
【注意】配置完1、2 后重启完配置 3。
```

## 2.10 robox绑核、绑numa、内存分配

```shell
在docker run命令中使用如下参数及示例，进行绑核、绑numa、内存分配
    //绑核
    --cpuset-cpus="${CPUS}"
    //绑numa
    --cpuset-mems=${MEM}
    //内存分配
    --memory="3584M"
【说明】numa node与CPU核数的对应关系可使用 numactl --hardware 查看
【示例】
    --cpuset-cpus="4-7"
    --cpuset-mems=0,1
    --memory="3584M"
```

# 三、鲲鹏920上启动robox安卓系统

```shell
//将发布包中aosp-on-anbox\binaryFiles目录下的启动脚本robox拷贝到/home/robox目录下
//修改/home/robox/目录下的robox启动脚本
# cp robox  /home/robox/
```

```shell
//鲲鹏920服务器上，执行修改后的robox脚本，启动robox安卓容器
# cd /home/robox
# export DISPLAY=:1    //【注意!!】robox脚本中设置的DISPLAY号要一致
//【注意】启动robox之前，先查看环境变量XDG_RUNTIME_DIR是否存在，若不存在，需要显示的export改环境变量，可以再robox可执行脚本开始位置中增加 export XDG_RUNTIME_DIR=/run/user/0，同时要确保/run/user/0目录存在
//启动脚本命令如下：
# ./robox -v start 1   //启动instance1
# ./robox -v start 2   //启动instance2

//查看docker实例进程
# docker ps 
CONTAINER ID        IMAGE                        COMMAND             CREATED             STATUS              PORTS                    NAMES
b77d371b402c        android:robox_with_exagear   "/anbox-init.sh"    13 seconds ago      Up 11 seconds       0.0.0.0:5561->5555/tcp   instance2
77b2c041315f        android:robox_with_exagear   "/anbox-init.sh"    2 hours ago         Up 2 hours          0.0.0.0:5559->5555/tcp   instance1

//查看主机session进程，主要查看instance1 instance2对应的两个session正常运行
# ps -aux | grep session
root       4330  0.0  0.0   9332  6160 ?        Ss   Oct22   0:01 /usr/bin/dbus-daemon --session --address=systemd: --nofork --nopidfile --systemd-activation --syslog-only
root     172678 22.1  0.0 6433328 250472 pts/8  Sl   19:51  25:38 anbox session-manager --run-multiple=instance1 --standalone --experimental --single-window --gles-driver=translator --window-size=720,1280
root     215155  1.4  0.0 5196228 185688 pts/8  Sl   21:46   0:01 anbox session-manager --run-multiple=instance2 --standalone --experimental --single-window --gles-driver=translator --window-size=720,1280

//确认robox实例是否启动成功
# docker exec -it instance1 sh  //登陆到安卓容器内
77b2c041315f:/ # getprop | grep sys.boot.completed
[sys.boot_completed]: [1] 
//sys.boot.completed显示为1,标识安卓启动完毕
```

```shell
//在windows系统中，调试时推荐使用ARDC版本软件，图形接入robox系统，版本获取参见该软件作者和博客，地址如下
原始作者博客链接：https://www.cnblogs.com/we-hjb/

//使用的一些注意事项如下：
//windows上需要连接的ip和端口，是鲲鹏920服务器上的ip和容器端口，如下面的5561端口
//在鲲鹏920上执行#docker ps获取实例的adb端口信息 
CONTAINER ID        IMAGE                        COMMAND             CREATED             STATUS              PORTS                    NAMES
b77d371b402c        android:robox_with_exagear   "/anbox-init.sh"    13 seconds ago      Up 11 seconds       0.0.0.0:5561->5555/tcp   instance2
//在windows上运行\ARDC(B1413)\目录下的ARDC.exe程序，菜单下“View”->"Mode"选择“Screenshot”模式，菜单"Devices"下选择adb已经连接的设备
//【注意】菜单“Key”下“Shutdown”、“Reboot”、“Home”按键不要去使用，存在问题。
```

```shell
//备选的投屏软件scrcpy
开源社区源码仓位置：https://github.com/Genymobile/scrcpy
开源社区版本发布位置：https://github.com/Genymobile/scrcpy/releases
```

```shell
//关闭启动的robox容器实例
# cd /home/robox
# ./robox -v stop 1   //关闭实例instance1
# ./robox -v stop 2   //关闭实例instance2
```

# 四、多GPU使能

```
//本章节描述如何使能服务器上的多块GPU，在启动robox时，将robox实例绑定到固定的GPU上
```

### 4.1 xorg绑定GPU

```
//每个GPU都要对应1个conf文件，例如插了2块GPU，则在/etc/X11下面创建xorg0.conf和xorg1.conf，conf文件和GPU通过PCI BUSID关联起来.
【说明】xorg0.conf和xorg1.conf可以根据实际情况存放，但需要在启动xorg时指定正确的路径，本文档中以/etc/X11目录为例
```

xorg0.conf:

```shell
Section "ServerFlags" 
Option "DontVTSwitch" "on" 
Option "AutoAddDevices" "off" 
Option "AutoEnableDevices" "off" 
Option "AutoAddGPU" "off" 
Option "AutoBindGPU" "off" 
EndSection 
Section "Device" 
Identifier "AMD" 
Driver "amdgpu" 
BusID "pci:1:00:00"
EndSection 
Section "Monitor" 
Identifier "monitor0" 
Option "enable" "true" 
#Modeline "1680x1050_60.00" 146.25  1680 1784 1960 2240  1050 1053 1059 1089 -hsync +vsync 
#Modeline "1024x768_60.00"  63.50  1024 1072 1176 1328  768 771 775 798 -hsync +vsync EndSection 
Section "Screen" 
Identifier "screen0" 
Device "AMD" 
Monitor "monitor0" 
DefaultDepth 24 
SubSection "Display" 
Depth 24 
#Modes "1680x1050_60.00" 
#Modes "1024x768_60.00" 
EndSubSection 
EndSection
```

xorg1.conf:

```shell
Section "ServerFlags"
Option "DontVTSwitch" "on"
Option "AutoAddDevices" "off"
Option "AutoEnableDevices" "off"
Option "AutoAddGPU" "off"
Option "AutoBindGPU" "off"
EndSection 
Section "Device"
Identifier "AMD"
Driver "amdgpu"
BusID "pci:2:00:00"
EndSection
Section "Monitor"
Identifier "monitor0"
Option "enable" "true" 
#Modeline "1680x1050_60.00" 146.25  1680 1784 1960 2240  1050 1053 1059 1089 -hsync +vsync 
#Modeline "1024x768_60.00"  63.50  1024 1072 1176 1328  768 771 775 798 -hsync +vsync EndSection 
Section "Screen"
Identifier "screen0"
Device "AMD"
Monitor "monitor0"
DefaultDepth 24
SubSection "Display"
Depth 24
#Modes "1680x1050_60.00"
#Modes "1024x768_60.00"
EndSubSection
EndSection
```

```shell
//配置文件中的BusID可通过如下方式获取：
# lspci |grep 5100
01:00.0 VGA compatible controller: Advanced Micro Devices, Inc. [AMD/ATI] Ellesmere [Radeon Pro WX 5100]
02:00.0 VGA compatible controller: Advanced Micro Devices, Inc. [AMD/ATI] Ellesmere [Radeon Pro WX 5100]
【注意】lspci看到的busid是十六进制的，而配置文件中的BusID里面的值是十进制的，所以需要做下转换
【注意】配置文件中的BusID格式是pci:xx:xx:xx，都是用“:”分隔的，而了lspci看到的busid的信息是xx:xx.x，一定不要直接拷贝lspci查到的busid去覆盖配置文件中的BusID。
```

### 4.2 xorg启动配置

```shell
# cd /home/robox
# vim robox
....
function start_framebuffer()
{
    if [[ "$VNC" != "true" ]]; then
        # This function is only relevant for VNC
        return
    fi

    display=":0"

    out "$instancenum: STARTING Frame Buffer"
    cmd="Xorg $display -config /etc/X11/xorg0.conf -nolisten tcp"

    # LEE: Might need a sleep here
    ps aux | grep Xorg | grep "Xorg[[:space:]]*$display " > /dev/null
    if [ $? -gt 0 ]; then
        warning "\e[01;31m  Create Xorg service\e[0m"
        debug $cmd
        eval $cmd &
        sleep 3
    else
        out "The Xorg service has been created \e[0m"
    fi
}
....
【说明】主要修改点：
       display=":0"
       cmd="Xorg $display -config /etc/X11/xorg0.conf -nolisten tcp"
这两个地方要对应起来，display=":0"对应xorg0.conf，display=":1"对应xorg1.conf，...
```

```shell
# cd /home/robox
# vim robox
....
function start_session_manager()
{
....
    echo $cmd
    export DISPLAY=:0
    eval $cmd &
    SESSIONMANAGERPID=$!

    disown
....
【说明】主要修改点：
       export DISPLAY=:0
改函数中的DISPLAY要与start_framebuffer函数中设置的display相同
```

### 4.3 robox启动

```
参照第三章
```

