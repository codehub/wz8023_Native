/*
 * Copyright (c) [2019] Huawei Technologies Co.,Ltd.All rights reserved.
 *
 * encTurbo is licensed under the Mulan PSL v1.
 * You can use this software according to the terms and conditions of the Mulan PSL v1.
 * You may obtain a copy of Mulan PSL v1 at:
 *
 *     http://license.coscl.org.cn/MulanPSL
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR
 * FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v1 for more details.
 */

#ifndef VA_ENCODE_API
#define VA_ENCODE_API

#include "va_encode_common.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * 功能说明：编码器上下文创建，编码之前必须先执行此接口，返回值作为后面接口的*encCt参数输入
 * 输入参数：
 *          avcencode_config_s encConfig 编码参数配置
 *          avcencode_input_s encInput 输入输出数据类型
 * 返回值：void * 编码库上下文指针，作为后面接口的参数输入
 */
void *EncTurboCreate(avcencode_config_s encConfig, avcencode_input_s encInput);

/**
 * 功能说明：编码器启动，执行之前需要先执行EncTurboCreate
 * 输入参数：void *encCtx 编码库上下文指针，EncTurboCreate接口返回的值
 * 返回值：ENCStatus 请参考ENCStatus定义
 */
ENCStatus EncTurboStart(void *encCtx);

/**
 * 功能说明：编码一帧；编码之前需要先启动编码库EncTurboStart，
 * 输入参数：void *encCtx 编码库上下文指针，EncTurboCreate接口返回的值
 *          unsigned long *srcBuffer，一帧数据地址，如果是RGB数据，则为从显存获取到RGB显存地址
 *          int frameIndex，帧序号，递增
 *          avcenc_codedbuf_s *codedBuf 输出数据返回
 * 返回值：ENCStatus 请参考ENCStatus定义
 */
ENCStatus EncTurboEncodeOneFrame(void *encCtx, unsigned long *srcBuffer, int frameIndex, avcenc_codedbuf_s *codedBuf);

/**
 * 功能说明：动态跟新编码参数
 * 输入参数：void *encCtx 编码库上下文指针，EncTurboCreate接口返回的值
 *          avcencode_config_s enConfig 支持frame_rate和bit_rate两个参数更新
 * 返回值：ENCStatus 请参考ENCStatus定义
 */
ENCStatus EncTurboUpdateEncodeParam(void *encCtx, avcencode_config_s encConfig);

/**
 * 功能说明：编码器停止，与EncTurboStart配对执行，执行之前需要先执行EncTurboCreate
 * 输入参数：void *encCtx 编码库上下文指针，EncTurboCreate接口返回的值
 * 返回值：ENCStatus 请参考ENCStatus定义
 */
ENCStatus EncTurboStop(void *encCtx);

/**
 * 功能说明：编码器销毁，与EncTurboCreate配套执行；执行之前需要先执行EncTurboCreate
 * 输入参数：void *encCtx 编码库上下文指针，EncTurboCreate接口返回的值
 * 返回值：ENCStatus 请参考ENCStatus定义
 */
ENCStatus EncTurboDestory(void *encCtx);

#ifdef __cplusplus
}
#endif
#endif
