/*
 * Copyright (c) [2019] Huawei Technologies Co.,Ltd.All rights reserved.
 *
 * encTurbo is licensed under the Mulan PSL v1.
 * You can use this software according to the terms and conditions of the Mulan PSL v1.
 * You may obtain a copy of Mulan PSL v1 at:
 *
 *     http://license.coscl.org.cn/MulanPSL
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR
 * FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v1 for more details.
 */


#include "va_encode_api.h"
#include "va_encode_impl.h"

void *EncTurboCreate(avcencode_config_s encConfig, avcencode_input_s encInput)
{
    return ContextCreate(encConfig, encInput);
}

ENCStatus EncTurboStart(void *encCtx)
{
    ENCStatus encStatus;
    encStatus = EncodeCreate(encCtx);
    if (encStatus != ENC_STATUS_SUCCESS) {
        return encStatus;
    }
    encStatus = EncodeOpen(encCtx);
    if (encStatus != ENC_STATUS_SUCCESS) {
        return encStatus;
    }
    encStatus = VppOpen(encCtx); 
    if (encStatus != ENC_STATUS_SUCCESS) {
        return encStatus;
    }
    return encStatus;
}

ENCStatus EncTurboEncodeOneFrame(void *encCtx, unsigned long *srcBuffer, int frameIndex, avcenc_codedbuf_s *codedBuf)
{
    return EncodeOneFrame(encCtx, frameIndex, srcBuffer, codedBuf);
}

ENCStatus EncTurboUpdateEncodeParam(void *encCtx, avcencode_config_s encConfig)
{
    return EncodeParamsUpdate(encCtx, encConfig);
}

ENCStatus EncTurboStop(void *encCtx)
{
    ENCStatus encStatus;

    encStatus = VppClose(encCtx);
    if (encStatus != ENC_STATUS_SUCCESS) {
        return encStatus;
    }
    encStatus = EncodeClose(encCtx);
    if (encStatus != ENC_STATUS_SUCCESS) {
        return encStatus;
    }
    encStatus = EncodeDestroy(encCtx);
    if (encStatus != ENC_STATUS_SUCCESS) {
        return encStatus;
    }
    return encStatus;
}

ENCStatus EncTurboDestory(void *encCtx)
{
    return ContextDestory(encCtx);
}
